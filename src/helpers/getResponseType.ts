import {ResponseTypes} from 'types/types';
import isNull from 'lodash/isNull';

const checkType = [
  (str: string) => str.includes('new: OK') ? ResponseTypes.NEW_OK : null,
  (str: string) => str.includes('open: You win') ? ResponseTypes.OPEN_WIN : null,
  (str: string) => str.includes('open: You lose') ? ResponseTypes.OPEN_FAIL : null,
  (str: string) => str.includes('open: OK') ? ResponseTypes.OPEN_SUCCESS : null,
  (str: string) => str.substr(0, 4) === 'map:' ? ResponseTypes.MAP : null,
];

const getResponseType = (data: string) => {
  return checkType.reduce<ResponseTypes | null>((result, callback) => isNull(result) ? callback(data) : result,
    null);
};

export default getResponseType;
