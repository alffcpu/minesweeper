import trim from 'lodash/trim';
import {GameMap} from 'types/types';
import fieldKey from './fieldKey';

const parseMap = (mapString: string) => {
  const mapRows = trim(mapString.substr(4)).split('\n');
  const sizeX = mapRows[0].length;
  const sizeY = mapRows.length;
  const gameMap: GameMap = {};
  mapRows.forEach((line, y) => {
    for(let x = 0; x <= sizeX; x++) {
      gameMap[fieldKey(x, y)] = line[x];
    }
  });
  return {
    map: gameMap,
    sizeX,
    sizeY
  };
};

export default parseMap;
