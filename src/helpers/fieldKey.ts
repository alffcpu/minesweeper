export default (x: number | string, y: number | string) =>
  `${x}_${y}`;
