import {HandlersActionTypes, IHandlersList} from 'types/types';

const socketEvents = (socket: WebSocket, handlers: IHandlersList, action: HandlersActionTypes) =>
  Object.entries(handlers).forEach(([eventNamem, handler]) => {
      if (!handler) return;
      return action === 'add' ?
        socket.addEventListener(eventNamem, handler) :
        socket.removeEventListener(eventNamem, handler);
    }
  );

export default socketEvents;
