import useStore from './useStore';
import {useCallback, useEffect} from 'react';
import {IHandlersList, ResponseTypes, SendCommand} from '../types/types';
import getResponseType from 'helpers/getResponseType';
import parseMap from 'helpers/parseMap';
import socketEvents from 'helpers/socketEvents';
import isNull from 'lodash/isNull';

export interface IUseSocketResult {
  started: boolean;
  connected: boolean;
  error: string;
  failed: boolean;
  initializing: boolean;
  sendCommand: SendCommand;
}

const useSocket = (): IUseSocketResult => {
  const {setStateValue, started, connected, error, failed, socket, autoOpen} = useStore();

  const setError = useCallback((errorText) => {
    setStateValue({connected: false, socket: null, error: errorText});
  }, [setStateValue]);

  const sendCommand: SendCommand = useCallback((command) => {
    if (socket) {
      socket.send(command);
    } else {
      setError(`Trying to send command to closed socket (${command})`);
    }
  }, [setError, socket]);

  useEffect(() => {
    const newSocket = new WebSocket(`${process.env.REACT_APP_WSS_URL}`);

    const handleOpen = () => {
      setStateValue({socket: newSocket});
    };
    const handleClose = ({reason}: CloseEvent) => {
      setStateValue({connected: false, socket: null});
      if (!!reason) setStateValue({error: reason});
    };
    const handleError = ({message}: ErrorEvent) => setStateValue({error: message});
    const handleMessage = ({data}: MessageEvent) => {
      const dataAsString = `${data}`;
      const action = getResponseType(dataAsString);
      switch (action) {
        case ResponseTypes.NEW_OK:
        case ResponseTypes.OPEN_SUCCESS:
          newSocket.send(`map`);
          break;
        case ResponseTypes.MAP:
          const {sizeX, sizeY, map} = parseMap(dataAsString);
          setStateValue((state) => ({
              ...state,
              connected: true,
              waiting: false,
              sizeX,
              sizeY,
              map,
              autoOpen: state.autoOpen ? state.autoOpen - 1 : state.autoOpen
            })
          );
          break;
        case ResponseTypes.OPEN_FAIL:
          setStateValue({failed: true, result: 'You lose!', waiting: false});
          break;
        case ResponseTypes.OPEN_WIN:
          setStateValue({failed: false, result: dataAsString.split('win.')[1], waiting: false});
          break;
      }
    };

    const handlers: IHandlersList = {open: handleOpen, close: handleClose, error: handleError, message: handleMessage};
    socketEvents(newSocket, handlers, 'add');

    return () => socketEvents(newSocket, handlers, 'remove');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    console.log('autoOpen: ', autoOpen);
    if (autoOpen === 1) {
      setTimeout(() => {
        setStateValue((state) => ({...state, solve: 1}));
        console.log('trying resolve again!');
      }, 1000);
    }
  }, [autoOpen]);

  return {
    started,
    connected,
    error,
    initializing: !error && isNull(socket),
    sendCommand,
    failed
  };
};

export default useSocket;
