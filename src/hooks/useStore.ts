import {useContext} from 'react';
import {AppStateContext} from 'state';

const useStore = () => useContext(AppStateContext);

export default useStore;
