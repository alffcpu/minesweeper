import useStore from './useStore';
import {useEffect} from 'react';

const useFlagMode = () => {
  const {setStateValue} = useStore();
  useEffect(() => {
    const flagModeHandler = (event: KeyboardEvent) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        setStateValue((state) => ({...state, flagMode: !state.flagMode}));
      }
    };
    window.addEventListener('keyup', flagModeHandler);
    return () => {
      window.removeEventListener('keypress', flagModeHandler);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

export default useFlagMode;
