import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import StateProvider from 'state';

import 'antd/dist/antd.css';
import './assets/styles.scss';
import 'react-virtualized/styles.css';

ReactDOM.render(
  <StateProvider>
    <App />
  </StateProvider>,
  document.getElementById('root')
);

