import React from 'react';
import {LoadingOutlined} from '@ant-design/icons';
import {Spin, Typography} from 'antd';

// □
const {Text} = Typography;
const Connector: React.FC = () => {
  return (<>
    <Typography>
      <Spin indicator={<LoadingOutlined className="connection-spinner" spin />} />
      <Text type="secondary"> Connecting</Text>
    </Typography>
  </>);
};

export default Connector;
