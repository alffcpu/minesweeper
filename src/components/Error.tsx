import React from 'react';
import {Alert} from 'antd';
import useStore from 'hooks/useStore';

const Error: React.FC = () => {
  const {error} = useStore();
  return (<Alert showIcon type="error" description={error} message="Error!" />);
};

export default Error;
