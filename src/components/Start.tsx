import React, {useCallback} from 'react';
import {Button, Select} from 'antd';
import { FlagOutlined } from '@ant-design/icons';
import useStore from 'hooks/useStore';
import {SendCommand} from 'types/types';

interface IStartProps {
  sendCommand: SendCommand;
}

const availableLevels = [1,2,3,4];

const { Option } = Select;
const Start: React.FC<IStartProps> = ({sendCommand}) => {
  const {level, setStateValue} = useStore();
  const handleSelect = useCallback((level) => setStateValue({level}), [setStateValue]);
  const handleStart = useCallback(() => {
    setStateValue({started: true, flags: {}, flagMode: false});
    sendCommand(`new ${level}`);
  }, [level, sendCommand, setStateValue]);
  return (<div className="start">
    <Select placeholder="Select level"
      defaultValue={level ? level : undefined}
      onChange={handleSelect}>
      {availableLevels.map((level) => <Option key={level} value={level}>Level {level}</Option>)}
    </Select>
    <Button disabled={!level}
      onClick={handleStart}
      type="primary"
      shape="round"
      icon={<FlagOutlined /> } size="large">Start the game</Button>
  </div>);
};

export default Start;
