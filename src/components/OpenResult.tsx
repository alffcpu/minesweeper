import React, {useCallback} from 'react';
import {Modal, Typography} from 'antd';
import useStore from 'hooks/useStore';

const {Title} = Typography;
const OpenResult: React.FC = () => {
  const {failed, setStateValue, result} = useStore();
  const handleRestartGame = useCallback(async () => {
    setStateValue({failed: false, result: '', started: false, map: null});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (<Modal
    title={result ? (failed ? 'Game Over' : 'Level Completed') : ''}
    visible={!!result}
    onOk={handleRestartGame}
    onCancel={handleRestartGame}
    cancelButtonProps={{style:{display: 'none'}}}
    okText={failed ? 'Restart Game' : 'Change Level'}
  >
    <Typography><Title level={3} type={failed ? 'danger' : 'warning'}>{result}</Title></Typography>
  </Modal>);
};

export default OpenResult;
