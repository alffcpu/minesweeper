import React, {useCallback} from 'react';
import {Layout, Typography} from 'antd';
import useStore from 'hooks/useStore';
import { CrownOutlined, FlagTwoTone, FlagFilled, CloudServerOutlined, TableOutlined, SettingOutlined} from '@ant-design/icons';

const {Title, Text} = Typography;

const Header: React.FC = () => {
  const {sizeX, sizeY, started, level, connected, error, setStateValue, flagMode, autoOpen} = useStore();
  const handleSolve = useCallback(() => {
    setStateValue({solve: 1});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const handleFlagged= useCallback(() => {
    setStateValue((state) => ({...state, flagMode: !state.flagMode}));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (<Layout.Header>
    <div className="header-content">
      <Typography>
        <Title>
          {process.env.REACT_APP_TITLE}
          {(!!level && started) &&
            <Text type="warning" className="level-info"><span onClick={handleSolve}><CrownOutlined /></span> {level}</Text>}
          {started &&
            <Text type="secondary" className={`flag-info ${flagMode ? 'flag-info-on' : ''}`}><span onClick={handleFlagged}>{flagMode ? <FlagFilled /> : <FlagTwoTone />}</span></Text>}
          {connected &&
            <Text {...(!!error  ? {type: 'danger'} : {className: 'text-success'})}><CloudServerOutlined /></Text>}
          {(!!sizeX && !!sizeY && started) &&
            <Text type="secondary"><TableOutlined /> {sizeX}x{sizeY}</Text>}
          {(started && autoOpen >= 1) &&
            <Text className="autosearch-info"><SettingOutlined spin /></Text>}
        </Title>
      </Typography>
    </div>
  </Layout.Header>);
};

export default Header;
