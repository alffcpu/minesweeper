import React, {useCallback} from 'react';
import useStore from 'hooks/useStore';
import {Spin} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';
import {FlagsMap, GameMap, SendCommand} from 'types/types';
import {AutoSizer, Grid, GridCellProps} from 'react-virtualized';
import fieldKey from 'helpers/fieldKey';

interface ISendCommandProps {
  sendCommand: SendCommand;
}

const overscanColumnCount = 5;
const overscanRowCount = 5;
const cellSize = 28;
export const unknownFieldValue = '□';
export const suggestedFieldValue = '*';

const noContentRenderer = () => <div className="gamemap-empty">No data!</div>;

const cellRender = (map: GameMap, flags: FlagsMap, {columnIndex, key, rowIndex, style}: GridCellProps, handleClick: (x: number, y: number) => void) => {
  const idx = fieldKey(columnIndex, rowIndex);
  const value = map[idx];
  const isFlagged = flags[idx];
  const baseCn = 'gamemap-cell';
  const cn = [baseCn];
  if (isFlagged) {
    cn.push(`${baseCn}-flagged`);
  }
  const isUnknown = value === unknownFieldValue;
  const isSuggested = value === suggestedFieldValue;
  if (value === '0') {
    cn.push(`${baseCn}-safe`);
  } else if (isSuggested) {
    cn.push(`${baseCn}-suggested`);
  } else if (!isUnknown) {
    cn.push(`${baseCn}-unsafe`);
  } else {
    cn.push(`${baseCn}-unknown`);
  }
  const onClick = isUnknown || isSuggested ?
    () => handleClick(columnIndex, rowIndex) :
    undefined;
  return <div onClick={onClick} className={cn.join(' ')} key={key} style={style}>
    {isUnknown ? (isFlagged ? ' ' : '?') : value}
  </div>;
};

const GameField: React.FC<ISendCommandProps> = ({sendCommand}) => {
  const {map, sizeY, sizeX, waiting, setStateValue, flagMode, flags} = useStore();
  const handleClick = useCallback((x, y) => {
    if (flagMode) {
      setStateValue((state) => {
        const flags = {...state.flags};
        const key = fieldKey(x, y);
        return {...state, map: {...state.map}, flags: {...flags, [key]: !flags[key]}};
      });
    } else {
      setStateValue({waiting: true});
      sendCommand(`open ${x} ${y}`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [flagMode]);
  const mapCellRender = useCallback((props) => cellRender(map ? map : {}, flags, props, handleClick), [flags, handleClick, map]);
  return <Spin spinning={waiting} indicator={<LoadingOutlined className="gamemap-spinner" spin />}>
    <div className={`gamemap-block ${flagMode ? 'flag-mode' : ''}`}>
      {map ?
        <AutoSizer>
          {({width, height}) => (
            <Grid
              cellRenderer={mapCellRender}
              columnWidth={cellSize}
              columnCount={sizeX}
              height={height}
              noContentRenderer={noContentRenderer}
              overscanColumnCount={overscanColumnCount}
              overscanRowCount={overscanRowCount}
              rowHeight={cellSize}
              rowCount={sizeY}
              width={width}
            />
          )}
        </AutoSizer> :
        null
      }
    </div>
  </Spin>;
};

export default GameField;
