import {GameMap, FlagsMap} from 'types/types';

export interface IState {
  connected: boolean;
  started: boolean;
  level: number;
  error: string;
  socket: WebSocket | null;
  map: GameMap | null;
  sizeX: number;
  sizeY: number;
  waiting: boolean;
  failed: boolean;
  result: string;
  flagMode: boolean;
  flags: FlagsMap;
  solve: 0 | 1 | 2;
  autoOpen: number;
  setStateValue: SetStateValueCallback;
  getState: GetStateCallback;
}

export const initialState: IState = {
  connected: false,
  started: false,
  socket: null,
  level: 0,
  error: '',
  map: null,
  sizeX: 0,
  sizeY: 0,
  waiting: false,
  failed: false,
  result: '',
  flagMode: false,
  flags: {},
  solve: 0,
  autoOpen: 0,
  setStateValue: () => undefined,
  getState: () => initialState,
};

type GetStateCallback = () => IState;
type SSVCallback = (state: IState) => IState;
export type SetStateValueCallback = (key: (keyof IState & string) | Partial<IState> | SSVCallback, value?: any) => void;
