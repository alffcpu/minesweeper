export interface IKeyValue<T = any> {
  [key: string]: T;
}

export type GameMap = {
  [key: string]: string;
};

export type FlagsMap = {
  [key: string]: boolean;
}

export type HandlersActionTypes = 'add' | 'remove';
type HandlersHandler<T> = (e: T) => void;
export interface IHandlersList {
  open?: HandlersHandler<Event>;
  close?: HandlersHandler<CloseEvent>;
  error?: HandlersHandler<ErrorEvent>;
  message?: HandlersHandler<MessageEvent>;
}

export enum ResponseTypes {
  NEW_OK,
  MAP,
  OPEN_SUCCESS,
  OPEN_FAIL,
  OPEN_WIN
}

export type SendCommand = (command: string) => void;
