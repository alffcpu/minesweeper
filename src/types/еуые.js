/**
 * Метод вносит корректировку в значения вероятностей нахождения мин в ячейках, учитывая взаимное влияние вероятностей соседних ячеек друг на друга
 */
private void correctPosibilities(){
	Map<Cell,Double>cells= new HashMap<>();
	// цикл устанавливает единое значение вероятности в каждой ячейке, учитывая различные значения вероятностей в ячейке от разных групп
	for (Group group : groups){
		for (Cell cell: group.getList()){
			Double value;
			if ((value=cells.get(cell))==null) // если ячейка еще не в мапе
				cells.put(cell,(double) group.getMines()/ group.size()); // то добавляем ее со значением из группы
		else     //если она уже в мапе, то корректируем ее значение по теории вероятности
			cells.put(cell,Prob.sum(value,(double) group.getMines()/ group.size()));
		}
	}
	// цикл корректирует значения с учетом того, что сумма вероятностей в группе должна быть равна количеству мин в группе
	boolean repeat;
	do{
		repeat=false;
		for (Group group : groups){                      // для каждой группы
			List<Double> prob= group.getProbabilities(); //  берем список вероятностей всех ячеек в группе в процентах
			Double sum=0.0;
			for (Double elem:prob)sum+=elem;             //  вычисляем ее сумму
			int mines= group.getMines()*100;             //  умножаем количество мин в группе на сто (из-за процентов)
			if (Math.abs(sum-mines)>1){                  //  если разница между ними велика, то проводим корректировку
				repeat=true;                             //   фиксируем факт корректировки
				Prob.correct(prob,mines);                //   корректируем список
				for (int i=0;i< group.size();i++){       //   заносим откорректированные значения из списка в ячейки
					double value= prob.get(i);
					group.getList().get(i).setPossibility(value);
				}
			}
		}
	}
	while (repeat);
	for (Cell cell:cells.keySet()){  // перестраховка
		if (cell.getPossibility()>99)cell.setPossibility(99);
		if (cell.getPossibility()<0)cell.setPossibility(0);
	}
}
