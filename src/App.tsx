import React, {useEffect} from 'react';
import {Layout, Skeleton} from 'antd';
import Start from 'components/Start';
import Header from 'components/Header';
import Connector from 'components/Connector';
import Error from 'components/Error';
import GameField from 'components/GameField';
import OpenResult from 'components/OpenResult';
import useSocket from './hooks/useSocket';
import useFlagMode from 'hooks/useFlagMode';
import useStore from 'hooks/useStore';
import {FlagsMap, GameMap, SendCommand} from 'types/types';

import {unknownFieldValue, suggestedFieldValue} from 'components/GameField';
import fieldKey from 'helpers/fieldKey';
import isNumber from 'lodash/isNumber';
import {SetStateValueCallback} from './state/store';

const countFlags = (flags: FlagsMap) => Object.values(flags).reduce<number>(
  (r, v) => v ? r + 1 : r, 0
);

const useSolver = (sendCommand: SendCommand) => {
  const {map, flags, solve, setStateValue, sizeX, sizeY} = useStore();
  useEffect(() => {
    if (solve === 1) {
      const [newFlags, newMap] = makeSuggestions(map || {}, flags, sizeX, sizeY);
      const wasFlagged = countFlags(flags);
      setStateValue({solve: 2, flags: newFlags, map: newMap});
      const nowFlagged = countFlags(newFlags);
      const flagsDiff = nowFlagged - wasFlagged;
      openSuggested(newMap, flagsDiff, sendCommand, setStateValue);
    }
  }, [solve, map]);
};

type ISnapshot = {
  [key: string]: number;
};

type CoordsArray = Array<[number, number]>;
type FunctionsArray = Array<() => any>;

const fieldUnknown = 10;
const filedMine = 9;

const openSuggested = (map: GameMap, flagsDiff: number, sendCommand: SendCommand, setStateValue: SetStateValueCallback) => {
  const suggested = Object.entries(map).reduce<FunctionsArray>((r, [key, value]) => {
    if (value === suggestedFieldValue) {
      return [
        ...r,
        () => {
          // setStateValue({waiting: true});
          sendCommand(`open ${key.replace('_', ' ')}`);
        }
      ];
    }
    return r;
  }, []);
  setStateValue({autoOpen: suggested.length});
  suggested.forEach((c) => c());
  if (!suggested.length && flagsDiff >= 1) {
    setStateValue((state) => ({...state, solve: 1}));
    console.log('rerun after flags changed!');
  }
};

const makeSuggestions = (map: GameMap, flags: FlagsMap, boardWidth: number, boardHeight: number): [FlagsMap, GameMap] => {
  const snapshot: ISnapshot = {};
  Object.entries(map).forEach(([key, value]) => {
    const numValue = value === unknownFieldValue ?
      fieldUnknown :
      parseInt(value, 10);
    snapshot[key] = flags[key] ?
      filedMine :
      numValue;
  });
  // const [newFlags, newMap] = inspect(snapshot, map, flags, boardWidth, boardHeight);
  // toConsole(snapshot, boardWidth, boardHeight);
  // toConsole(newFlags, boardWidth, boardHeight);
  // return [newFlags, newMap];
  return inspect(snapshot, map, flags, boardWidth, boardHeight);
};

const toConsole = (snapshot: ISnapshot | FlagsMap, w: number, h: number) => {
  let str = '';
  for (let y = 0; y < h; y++) {
    for (let x = 0; x < w; x++) {
      const idx = fieldKey(x, y);
      const value = snapshot[idx];
      if (isNumber(value)) {
        str += snapshot[idx] === 10 ? 'A' : snapshot[idx];
      } else {
        str += value ? 'Y' : 'N';
      }
    }
    str += '\n';
  }
  console.log(str);
  console.log('---');
};

const sides = [[-1,0], [-1,-1], [0,-1], [1,-1], [1,0], [1,1], [0,1], [-1,1]];

const inspect = (snapshot: ISnapshot, map: GameMap, flags: FlagsMap, boardWidth: number, boardHeight: number): [FlagsMap, GameMap] => {
  const inspected: ISnapshot = {...snapshot};
  const newMap = {...map};
  const result: FlagsMap = {...flags};
  const getInformation = (x: number, y: number): number => {
    const idx = fieldKey(x, y);
    return typeof inspected[idx] !== 'undefined' ? inspected[idx] : fieldUnknown;
  };
  const isValidX = (x: number) => x >= 0 && x < boardWidth;
  const isValidY = (y: number) => y >= 0 && y < boardHeight;
  const getClosedMines = (x: number, y: number) => {
    const closed: CoordsArray = [];
    const mines: CoordsArray = [];
    sides.forEach(([addX, addY]) => {
      const cx = x + addX;
      const cy = y + addY;
      const cIdx = fieldKey(cx, cy);
      if (!isValidX(x) || !isValidY(y)) {
        return;
      } else if (inspected[cIdx] === 10) {
        closed.push([cx, cy]);
      } else if (inspected[cIdx] === 9) {
        mines.push([cx, cy]);
      }
    });
    return [closed, mines];
  };

  for (let y = 0; y < boardHeight; y++) {
    for (let x = 0; x < boardWidth; x++) {
      const value = getInformation(x, y);
      if (value > 0 && value < 9) {
        const [closed, mines] = getClosedMines(x, y);
        if (closed.length > 0) {
          if (value === mines.length) {
            //closed are safe
            closed.forEach(([sx, sy]) => newMap[fieldKey(sx, sy)] = suggestedFieldValue);
          }
          if (value === (mines.length + closed.length)) {
            // closed are mines
            closed.forEach(([mx, my]) => result[fieldKey(mx, my)] = true);
          }
        }
      }
    }
  }
  return [result, newMap];
};

const {Content} = Layout;
const App: React.FC = () => {
  const {started, connected, error, initializing, sendCommand} = useSocket();
  useFlagMode();
  useSolver(sendCommand);
  return(<Layout>
    <Header />
    <Content>
      {initializing ?
        <Skeleton active /> :
        <>
          {!started && <Start sendCommand={sendCommand} />}
          {(started && !connected && !error) && <Connector/>}
          {(started && connected && !error) && <GameField sendCommand={sendCommand} />}
          {!!error && <Error />}
          <OpenResult />
        </>}
      </Content>
  </Layout>);
};

export default App;
