# TASK #

Your task is to solve all levels of the game available at https://hometask.eg1236.com/game1/  
  
· 4 passwords from the game  
· Link to a private GitHub repository with the code you wrote in order to solve the game. Give user evo-home-task (or dev-home-task@evolutiongaming.com) access to the repository  
  
